# How to use

1.  Add Clockify API key and add test or un-comment MM `WORKSPACE_ID` to `.env` file

`.env` example file
```sh
#Actual MM workspace ID
# WORKSPACE_ID=5d60394170822771b7ac5d1e

#Test (If you create a workspace for dev)
#WORKSPACE_ID=make-a-test-workspace  

MY_CLOCKIFY_API_KEY=your-api-key-goes-here   
```
 
2. run `npm install`

3. run `node clockify.js`
