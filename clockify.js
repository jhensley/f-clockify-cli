require('dotenv').config();

const { submitWeek } = require('./api/timeEntry');
const welcomeMessage = require('./cli/welcome');
const { selectProjects } = require('./cli/selectProjects');
const { getHoursPerWeek } = require('./cli/hours');
const { getAllocation } = require('./cli/allocation');
const { getTargetWeek } = require('./cli/targetWeek');
const { confirm } = require('./cli/confirm');

const fuckClockify = async () => {
    welcomeMessage();

    const targetMonday = await getTargetWeek();

    const hoursPerWeek = await getHoursPerWeek();
    const hoursPerDay = hoursPerWeek / 5;

    const projects = await selectProjects();
    const projectsWithTime = await getAllocation(hoursPerDay, projects);

    const hasBeenConfirmed = confirm(projectsWithTime);

    if (hasBeenConfirmed) {
        submitWeek(targetMonday)(projectsWithTime);
    }
};

fuckClockify();
