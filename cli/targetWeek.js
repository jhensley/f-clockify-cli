const chalk = require('chalk');
const { prompt } = require('inquirer');
const { startOfWeek, addDays, format, isValid } = require('date-fns');

const dateFmt = 'MM/dd/yy';

const invalidDateError = inputDate =>
    console.log(
        chalk.red(`${inputDate} is an invalid date, enter a valid date.`)
    );

const getMonday = referenceDate => addDays(startOfWeek(referenceDate), 1);

const weekRangeFormat = monday => {
    const friday = addDays(startOfWeek(monday), 5);

    return chalk.magenta(
        `${format(monday, dateFmt)} - ${format(friday, dateFmt)}`
    );
};

const inputCustomWeek = async () => {
    let { inputDate } = await prompt({
        name: 'inputDate',
        message: `Enter the week that you'd like to enter time for. ${chalk.grey(
            'If a Monday is not entered, last occurring Monday will be calculated.'
        )} MM/dd/yyyy preferred i.e. ${chalk.magenta('3/19/2021')}. `,
        type: 'input'
    });

    const date = new Date(inputDate);

    if (!isValid(date)) {
        invalidDateError(inputDate);
        return await inputCustomWeek();
    }

    return getMonday(date);
};

const getTargetWeek = async () => {
    const thisMonday = getMonday(new Date());

    const { thisWeek } = await prompt({
        name: 'thisWeek',
        message: `Is this for the week of ${weekRangeFormat(thisMonday)}?`,
        type: 'confirm'
    });

    if (thisWeek) {
        return thisMonday;
    }

    return await inputCustomWeek();
};

module.exports = {
    getTargetWeek
};
