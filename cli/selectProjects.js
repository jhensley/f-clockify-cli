const chalk = require('chalk');
const { prompt } = require('inquirer');
const { getActiveProjects } = require('../api/workspaces');

const selectProjects = async () => {
    const allProjects = await getActiveProjects();

    const { projects } = await prompt({
        name: 'projects',
        message: 'Which projects did you work on this week?',
        type: 'checkbox',
        choices: allProjects.map(({ name, id, color }) => {
            return {
                name: chalk.hex(color).bold(name),
                value: { name, id, color }
            };
        })
    });

    return projects;
};

module.exports = {
    selectProjects
};
