const chalk = require('chalk');
const { prompt } = require('inquirer');
const Table = require('cli-table3');

const table = new Table({
    head: ['Project', 'Time']
});

const confirm = async projectsWithHours => {
    // const formattedData = projectsWithHours.map(({ name, durationInHours }) => [
    //     name,
    //     durationInHours
    // ]);

    for (const project of projectsWithHours) {
        table.push([project.name, project.durationInHours]);
    }

    console.log(table.toString());

    const { shouldSubmit } = await prompt({
        name: 'shouldSubmit',
        message: 'Would you like to submit this data?',
        type: 'confirm'
    });

    return shouldSubmit;
};

module.exports = {
    confirm
};
