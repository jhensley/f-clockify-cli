const chalk = require('chalk');
const { prompt } = require('inquirer');

const getTimePercentage = async ({ name, color }) => {
    const { percentage } = await prompt({
        name: 'percentage',
        message: `How much time did you spend on ${chalk
            .hex(color)
            .bold(
                name
            )} this week? (Percentage out of 100, e.g. if 10% enter 10)`,
        type: 'input'
    });

    return percentage;
};

const getAllocation = async (hoursPerDay, projects) => {
    const { isSplitEvenly } = await prompt({
        name: 'isSplitEvenly',
        message: 'Was time split evenly between these projects?',
        type: 'confirm'
    });

    if (isSplitEvenly) {
        return projects.map(project => ({
            durationInHours: hoursPerDay / projects.length,
            ...project
        }));
    }

    const allocatedProjects = [];

    for (const project of projects) {
        const timePercentage = await getTimePercentage(project);

        allocatedProjects.push({
            durationInHours: hoursPerDay * (timePercentage / 100),
            ...project
        });
    }

    return allocatedProjects;
};

module.exports = {
    getAllocation
};
