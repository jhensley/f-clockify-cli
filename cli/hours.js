const chalk = require('chalk');
const { prompt } = require('inquirer');

const enterCustomHours = async () => {
    const { hours } = await prompt({
        name: 'hours',
        message: 'How many hours did you work this week?',
        type: 'input'
    });

    return hours;
};

const getHoursPerWeek = async () => {
    const { isStandardHours } = await prompt({
        name: 'isStandardHours',
        message: `Did you work a ${chalk.magenta(40)} hour work week?`,
        type: 'confirm'
    });

    if (isStandardHours) {
        return 40;
    }

    return enterCustomHours();
};

module.exports = {
    getHoursPerWeek
};
