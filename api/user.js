const clockify = require('./index');

const getMyUserId = async () => {
    const { data } = await clockify.get('/user');
    return data.id;
};

module.exports = {
    getMyUserId
};
