const { startOfDay, addHours, addDays } = require('date-fns');
const clockify = require('./index');

const submitTime = projectId => (date, durationInHours) => {
    const startOfWorkDay = addHours(startOfDay(date), 9);

    return clockify.post(
        `/workspaces/${process.env.WORKSPACE_ID}/time-entries`,
        {
            start: startOfWorkDay,
            end: addHours(startOfWorkDay, durationInHours),
            projectId
        }
    );
};

const submitWeek = targetMonday => projectsWithTime => {
    for (let i = 0; i < 5; i++) {
        for (const project of projectsWithTime) {
            const targetDay = addDays(targetMonday, i);
            submitTime(project.id)(targetDay, project.durationInHours).then(
                ({ data }) => {
                    console.log(data);
                }
            );
        }
    }
};

module.exports = {
    submitTime,
    submitWeek
};
