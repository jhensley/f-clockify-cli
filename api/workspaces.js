const clockify = require('./index');

// AFTER DEBUGGING DELETE THIS
const getWorkspaceId = async () => {
    const workspaces = await clockify.get('/workspaces');
    console.log(workspaces);
    return workspaces[0].id;
};

const getAllProjects = async () => {
    const { data } = await clockify.get(
        `/workspaces/${process.env.WORKSPACE_ID}/projects`
    );

    return data;
};

const getActiveProjects = async () => {
    const allProjects = await getAllProjects();
    return allProjects.filter(({ archived }) => !archived);
};

module.exports = {
    getWorkspaceId,
    getAllProjects,
    getActiveProjects
};
