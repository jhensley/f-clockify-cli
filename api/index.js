const axios = require('axios');

const BASE_URL = 'https://api.clockify.me/api/v1';

const clockify = axios.create({
    baseURL: BASE_URL,
    headers: { 'X-Api-Key': process.env.MY_CLOCKIFY_API_KEY }
});

module.exports = clockify;
